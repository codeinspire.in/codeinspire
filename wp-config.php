<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'codeinspire' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mR>76 6yQ/}N@-V<(1aqAC0z}Sq8M+m>g:d~W55d@=YY ?<rWi% qaQ}wBV?Uv!c' );
define( 'SECURE_AUTH_KEY',  '.K0}v22<+P .j+Gn,zAWy6w>$OC!x[=rqu%iMW<L&/<U[yk@7PcZKM[EP6jjfHIR' );
define( 'LOGGED_IN_KEY',    'hK@J3{p zBL6kFV1A`}R7b$+CqhN9f8=?,bRFLxlq$%H_fi)d,b1hJ{*0[,b[|_[' );
define( 'NONCE_KEY',        '[YmNXX>-6RT;<6NGq+Y#uAb>OYPZ#lRmPB>f^%_/.g^>/l 2_{P!LshH4*!dJfTz' );
define( 'AUTH_SALT',        '?^KFchtaQ0)mQi:39hXE ?bG0dyUE!HC/J k(0gL0]p?Edw+i>gLiTU4XP2VNs!R' );
define( 'SECURE_AUTH_SALT', '@*h$A#).2nv(VMCXeSTc8udr~w$),d$RS&xx>eD{n+]VcmrD>Yk!b(=@/P(]XHVe' );
define( 'LOGGED_IN_SALT',   'E}7TZ_HB@W(u,,XhTM=(g<3vw!4m2(O&:@PG*;],nGGnH6]%K/Y)%;dnC$mc,OgB' );
define( 'NONCE_SALT',       '}>1^@B`?/?XZj]3FR<K.36c5rcE{AdHth=./@VALS*Aw5v n&z]pF[I(f9=OBMjf' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
